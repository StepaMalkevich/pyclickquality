# Quality of Click Models for Web Search #


### Running examples ###

* examples in quality/example
* run step_1 and get two files with real sessions in JSON format and predicted sessions by DBN model in JSON format
* you can watch this 2 files in relevant directory in data
* run step_2 to read files from JSON and measure MAE and MSE metrics
* run step_3 to get mean of this metrics