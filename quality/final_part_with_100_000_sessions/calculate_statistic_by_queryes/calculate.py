import glob
from pyclick.search_session import SearchSession
import numpy as np
import time
import scipy.stats
import pickle


def get_session_from_JSON_line(line):
    entry_array = line.strip().split("\t")
    index, session_in_json = entry_array[0], entry_array[1]
    session = SearchSession(None).from_JSON(session_in_json)

    return index, session


def number_of_clicks(query_session, model_name):
    arr = np.zeros((11,))

    cc = np.sum(np.array(query_session.get_clicks()))

    arr[cc] += 1

    return arr


def clicks_array(query_session, model_name):
    if (model_name == "first_simple"):
        return np.zeros((10,))

    if (model_name == "second_simple"):
        arr = np.zeros((10,))
        arr[0] = 1

        return arr

    arr = np.array(query_session.get_clicks())

    return arr


def save_statistics(model_name):
    list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")
    results_by_query_file = open("/Users/Stepan/Desktop/results_by_query/" + model_name + "_test_res_query.txt", "w")

    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "r")

    q_set = set()
    for line in file_with_query_from_test:
        line = line.strip()
        q_set.add((int)(line))

    global_arr = np.zeros((21433536, 10))

    for name in list_of_names:
        start = time.time()
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                query = int(query_session.query)

                global_arr[query] += clicks_array(query_session)


        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

        end = time.time()
        print "Work time is", (end - start) / 60, 'min'

    s1, s2 = global_arr.shape

    for q in np.arange(s1):
        if (q in q_set):
            results_by_query_file.write(str(q) + "->" + str(global_arr[q]) + "\n")


def get_clicks_statistics(model_name):
    if (model_name == "first_simple" or model_name == "second_simple"):
        list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/real_test_JSON_days/*")
    else:
        list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")

    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "r")

    q_set = set()
    for line in file_with_query_from_test:
        line = line.strip()
        q_set.add((int)(line))

    global_arr = np.zeros((21433536, 10), dtype=np.float)
    global_arr_cnt = np.zeros((21433536,), dtype=np.float)

    q_cc = 0
    cc_one_click = 0

    print(model_name)
    for name in list_of_names:
        start = time.time()
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                query = int(query_session.query)

                local_clicks_array = clicks_array(query_session, model_name)

                if (local_clicks_array[0] == 1):
                    cc_one_click += 1

                global_arr[query] += local_clicks_array

                global_arr_cnt[query] += 1
                q_cc += 1


        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

        end = time.time()
        print "Work time is", (end - start) / 60, 'min'

    print("pres of sess", (float)(cc_one_click)/q_cc)
    print(cc_one_click)
    print(q_cc)
    return global_arr, global_arr_cnt


def get_number_of_clicks_statistic(model_name):
    list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")

    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "r")

    q_set = set()
    for line in file_with_query_from_test:
        line = line.strip()
        q_set.add((int)(line))

    global_arr = np.zeros((21433536, 11), dtype=np.float)
    global_arr_cnt = np.zeros((21433536,), dtype=np.float)

    query_cc = 0
    print(model_name)
    for name in list_of_names:
        start = time.time()
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                query = int(query_session.query)
                query_cc += 1

                local_clicks_array = number_of_clicks(query_session, model_name)

                global_arr[query] += local_clicks_array

                global_arr_cnt[query] += 1


        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

        end = time.time()
        print "Work time is", (end - start) / 60, 'min'

    print("query_cc", query_cc)
    return global_arr, global_arr_cnt


def save_div_to_file(div_arr):
    file = open("/Users/Stepan/Desktop/div_arr.txt", "w")

    for elem in div_arr:
        file.write(str(elem) + "\n")


def calculate_mean_div(model_1_arr, model_1_arr_cnt, model_2_arr, model_2_arr_cnt):
    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "r")

    q_set = set()
    for line in file_with_query_from_test:
        line = line.strip()
        q_set.add((int)(line))

    n = 0

    sum_div_rare = 0
    sum_div_normal = 0
    sum_div_freq = 0

    rare_cc = 0
    norm_cc = 0
    freq_cc = 0

    sum_div = 0

    for q in q_set:
        arr_1 = model_1_arr[q]
        arr_2 = model_2_arr[q]

        arr_1 = arr_1.astype(float)
        arr_2 = arr_2.astype(float)

        coef_1 = (float)(model_1_arr_cnt[q])
        coef_2 = (float)(model_1_arr_cnt[q])

        if (coef_1 != coef_2):
            max_coef = max(coef_1, coef_2)
            coef_1 = max_coef
            coef_2 = max_coef

        arr_1 = arr_1 * coef_1
        arr_2 = arr_2 * coef_2

        arr_1[arr_1 == 0] = 1e-5
        arr_2[arr_2 == 0] = 1e-5

        p = arr_2
        q = arr_1

        div = scipy.stats.entropy(p, q)

        if (coef_1 < 10):
            sum_div_rare += div
            rare_cc += coef_1

        elif (coef_1 >= 10 and coef_1 < 100):
            sum_div_normal += div
            norm_cc += coef_1

        else:
            sum_div_freq += div
            freq_cc += coef_1

        sum_div += div
        n += coef_1

    return sum_div / n


def calculate_mean_div_clicks(model_1_arr, model_1_arr_cnt, model_2_arr, model_2_arr_cnt):
    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "r")

    q_set = set()
    for line in file_with_query_from_test:
        line = line.strip()
        q_set.add((int)(line))

    sum_div = 0
    sum_div_first = 0
    sum_div_second = 0
    sum_div_third = 0
    sum_div_last = 0

    first_cc = 0
    second_cc = 0
    third_cc = 0
    last_cc = 0

    n = 0

    for q in q_set:
        arr_1 = model_1_arr[q]
        arr_2 = model_2_arr[q]

        arr_1 = arr_1.astype(float)
        arr_2 = arr_2.astype(float)

        coef_1 = (float)(model_1_arr_cnt[q])
        coef_2 = (float)(model_1_arr_cnt[q])

        if (coef_1 != coef_2):
            max_coef = max(coef_1, coef_2)
            coef_1 = max_coef
            coef_2 = max_coef

        mean_number_of_clicks_in_real = np.sum(arr_1) / float(10 * coef_1)

        arr_1 = arr_1 * coef_1
        arr_2 = arr_2 * coef_2

        arr_1[arr_1 == 0] = 1e-5
        arr_2[arr_2 == 0] = 1e-5

        p = arr_2
        q = arr_1

        div = scipy.stats.entropy(p, q)

        if (mean_number_of_clicks_in_real > 0 and mean_number_of_clicks_in_real <= 0.1):
            sum_div_first += div
            first_cc += coef_1

        elif (mean_number_of_clicks_in_real > 0.1 and mean_number_of_clicks_in_real <= 0.2):
            sum_div_second += div
            second_cc += coef_1

        elif (mean_number_of_clicks_in_real > 0.2 and mean_number_of_clicks_in_real <= 0.3):
            sum_div_third += div
            third_cc += coef_1

        else:
            sum_div_last += div
            last_cc += coef_1

        sum_div += div
        n += coef_1

    print("first_div", np.round(sum_div_first / first_cc,2))
    print("second_div", np.round(sum_div_second / second_cc,2))
    print("third_div", np.round(sum_div_third / third_cc,2))
    print("last_div", np.round(sum_div_last / last_cc,2))

    return sum_div / n


def get_clicks_for_hist(model_name):
    model_arr, model_arr_cnt = get_clicks_statistics(model_name)

    # model_sum_freq = np.sum(model_arr[model_arr_cnt >= 100, 0:], axis=0)

    # model_sum = np.sum(model_arr, axis=0)


def save_model_arr(model_arr, model_name):
    with open(model_name + '.pickle', 'wb') as f:
        pickle.dump(model_name, f)


def read_model_arr(model_name):
    with open(model_name + '.pickle', 'rb') as f:
        return pickle.load(f)


real_arr, real_arr_cnt = get_clicks_statistics("real")
#model_arr, model_arr_cnt = get_clicks_statistics("CCM_pred")

#print("MKL div", calculate_mean_div_clicks(real_arr, real_arr_cnt, model_arr, model_arr_cnt))
