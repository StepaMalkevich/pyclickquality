import numpy as np

def show_quality_for_model(model_name):
    result_file = open("/Users/Stepan/Desktop/metric_result/last_result_of_" + model_name +".txt", 'r')

    r_first_sum = float(0)
    r_last_sum = float(0)
    mse_sum = float(0)
    cc = float(0)
    all_cc = float(0)

    flag = True

    for line in result_file:
        line = line.strip()
        lines = line.split("\t")
        query, res_1, res_2, mse = lines

        if (flag):
            if (res_1 == 'None'):

                if (res_2 == '(None, None)'):
                    r_first_sum += 0
                    r_last_sum += 0
                else:

                    data = res_2.split(', ')

                    r_first = (float)(data[0][1])
                    r_last = (float)(data[1][0])

                    r_first_sum += r_first
                    r_last_sum += r_last



            else:
                r_first = (float)(res_1)
                r_last = (float)(res_2)

                r_first_sum += r_first
                r_last_sum += r_last

            cc += 1


        else:
            if (res_1 == 'None'):
                if (res_2 == '(None, None)'):
                    r_first_sum += 0
                    r_last_sum += 0

                    cc += 1
            else:
                r_first = (float)(res_1)
                r_last = (float)(res_2)

                r_first_sum += r_first
                r_last_sum += r_last

                cc += 1

        mse_sum += (float)(mse)
        all_cc += 1

    r_first_mean = r_first_sum / cc
    r_last_mean = r_last_sum / cc
    mse_mean = mse_sum / all_cc

    print 'MAE in first clicked position:', np.round(r_first_mean,2)
    print 'MAE in last clicked position:', np.round(r_last_mean,2)
    print 'MSE:', mse_mean

show_quality_for_model("second_simple_pred")