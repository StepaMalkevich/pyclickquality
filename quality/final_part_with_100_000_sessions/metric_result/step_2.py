import glob
import numpy as np
from pyclick.search_session import SearchSession


def mae_first_last_clicks(pred, real):
    """
    Mean Absolute Error of first and last real and predicted clicks.

    :returns: Two MAE.
    """
    flag_pred = True
    flag_real = True

    r_first_pred = None
    r_first_real = None
    r_last_pred = None
    r_last_real = None

    for i in range(len(pred)):
        if (flag_pred and pred[i] == 1):
            r_first_pred = i + 1
            flag_pred = False
        if (flag_real and real[i] == 1):
            r_first_real = i + 1
            flag_real = False

        if ((not flag_pred) and pred[i] == 1):
            r_last_pred = i + 1
        if ((not flag_real) and real[i] == 1):
            r_last_real = i + 1

    if (r_first_real == None):
        return None, (r_first_pred, r_last_pred)

    if (r_first_pred == None):
        return None, (r_first_real, r_last_real)

    return abs(r_first_pred - r_first_real), abs(r_last_pred - r_last_real)


def mean_squared_error(pred, real):
    """
    Mean Squared Error of real and predicted clicks.

    :returns: MSE.
    """
    pred = np.array(pred)
    real = np.array(real)

    return ((pred - real) ** 2).mean(axis=0)


list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/real_test_JSON_days/*")


def get_session_from_JSON_line(line):
    entry_array = line.strip().split("\t")
    index, session_in_json = entry_array[0], entry_array[1]
    session = SearchSession(None).from_JSON(session_in_json)

    return index, session


def save_metric_results(model_name):
    result_file = open("/Users/Stepan/Desktop/metric_result/result_of_" + model_name +".txt", 'w')

    for name in list_of_names:
        print('Enter in' + name)
        day = name[-6:-4]
        real_test_JSON_file = open(name, 'r')
        model_pred_test_JSON_file = open(
            "/Users/Stepan/Desktop/data_100_000/" + model_name + "_pred_test_JSON_days/" + model_name + "_pred_test_JSON_day_" + day + ".txt",
            'r')
        try:
            while True:
                real_JSON_line = real_test_JSON_file.next()
                pred_JSON_line = model_pred_test_JSON_file.next()

                index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)
                index_in_pred, pred_session = get_session_from_JSON_line(pred_JSON_line)

                real_clicks = real_session.get_clicks()
                pred_clicks = pred_session.get_clicks()

                if (index_in_real == index_in_pred and real_session.query == pred_session.query and real_session.task == pred_session.task):
                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'
                    result_file.write(result)

                elif (real_session.query == pred_session.query and real_session.task == pred_session.task):
                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'
                    result_file.write(result)
                else:
                    real_JSON_line = real_test_JSON_file.next()
                    index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)

                    while (real_session.query != pred_session.query or real_session.task != pred_session.task):
                        print('real', index_in_real, 'pred', index_in_pred)
                        real_JSON_line = real_test_JSON_file.next()
                        index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)

                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'
                    result_file.write(result)

        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

    result_file.close()

def clicks_array(query_session, model_name):
    if (model_name == "first_simple"):
        return np.zeros((10,))

    if (model_name == "second_simple"):
        arr = np.zeros((10,))
        arr[0] = 1

        return arr

    arr = np.array(query_session.get_clicks())

    return arr

def get_clicks_statistics(model_name):
    if (model_name == "first_simple" or model_name == "second_simple"):
        list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/real_test_JSON_days/*")
    else:
        list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")

    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "r")

    q_set = set()
    for line in file_with_query_from_test:
        line = line.strip()
        q_set.add((int)(line))

    global_arr = np.zeros((21433536, 10), dtype=np.float)
    global_arr_cnt = np.zeros((21433536,), dtype=np.float)

    print(model_name)
    for name in list_of_names:
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                query = int(query_session.query)

                local_clicks_array = clicks_array(query_session, model_name)

                global_arr[query] += local_clicks_array

                global_arr_cnt[query] += 1


        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

    return global_arr, global_arr_cnt


def save_metric_results_frequence(model_name):
    rare_result_file = open("/Users/Stepan/Desktop/metric_result/rare_result_of_" + model_name +".txt", 'w')
    normal_result_file = open("/Users/Stepan/Desktop/metric_result/normal_result_of_" + model_name +".txt", 'w')
    freq_result_file = open("/Users/Stepan/Desktop/metric_result/freq_result_of_" + model_name +".txt", 'w')

    _, global_arr = get_clicks_statistics(model_name)

    for name in list_of_names:
        print('Enter in' + name)
        day = name[-6:-4]
        real_test_JSON_file = open(name, 'r')
        model_pred_test_JSON_file = open(
            "/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/" + model_name + "_test_JSON_day_" + day + ".txt",
            'r')
        try:
            while True:
                real_JSON_line = real_test_JSON_file.next()
                pred_JSON_line = model_pred_test_JSON_file.next()

                index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)
                index_in_pred, pred_session = get_session_from_JSON_line(pred_JSON_line)

                real_clicks = real_session.get_clicks()
                pred_clicks = pred_session.get_clicks()

                if (index_in_real == index_in_pred and real_session.query == pred_session.query and real_session.task == pred_session.task):
                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'

                    number_of_query = global_arr[(int)(real_session.query)]

                    if (number_of_query < 10):
                        rare_result_file.write(result)
                    elif (number_of_query >= 10 and number_of_query < 100):
                        normal_result_file.write(result)
                    else:
                        freq_result_file.write(result)

                elif (real_session.query == pred_session.query and real_session.task == pred_session.task):
                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'
                    number_of_query = global_arr[(int)(real_session.query)]

                    if (number_of_query < 10):
                        rare_result_file.write(result)
                    elif (number_of_query >= 10 and number_of_query < 100):
                        normal_result_file.write(result)
                    else:
                        freq_result_file.write(result)
                else:
                    real_JSON_line = real_test_JSON_file.next()
                    index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)

                    while (real_session.query != pred_session.query or real_session.task != pred_session.task):
                        print('real', index_in_real, 'pred', index_in_pred)
                        real_JSON_line = real_test_JSON_file.next()
                        index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)

                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'
                    number_of_query = global_arr[(int)(real_session.query)]

                    if (number_of_query < 10):
                        rare_result_file.write(result)
                    elif (number_of_query >= 10 and number_of_query < 100):
                        normal_result_file.write(result)
                    else:
                        freq_result_file.write(result)

        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

    rare_result_file.close()
    normal_result_file.close()
    freq_result_file.close()


def save_metric_results_mean_clicks(model_name):
    first_result_file = open("/Users/Stepan/Desktop/metric_result/first_result_of_" + model_name +".txt", 'w')
    second_result_file = open("/Users/Stepan/Desktop/metric_result/second_result_of_" + model_name +".txt", 'w')
    third_result_file = open("/Users/Stepan/Desktop/metric_result/third_result_of_" + model_name +".txt", 'w')
    last_result_file = open("/Users/Stepan/Desktop/metric_result/last_result_of_" + model_name +".txt", 'w')

    global_arr, global_arr_cnt = get_clicks_statistics("real")

    for name in list_of_names:
        print('Enter in' + name)
        day = name[-6:-4]
        real_test_JSON_file = open(name, 'r')
        model_pred_test_JSON_file = open(
            "/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/" + model_name + "_test_JSON_day_" + day + ".txt",
            'r')
        try:
            while True:
                real_JSON_line = real_test_JSON_file.next()
                pred_JSON_line = model_pred_test_JSON_file.next()

                index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)
                index_in_pred, pred_session = get_session_from_JSON_line(pred_JSON_line)

                real_clicks = real_session.get_clicks()
                pred_clicks = pred_session.get_clicks()

                if (index_in_real == index_in_pred and real_session.query == pred_session.query and real_session.task == pred_session.task):
                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'

                    arr_1 = global_arr[(int)(real_session.query)]
                    coef_1 = (float)(global_arr_cnt[(int)(real_session.query)])

                    mean_number_of_clicks_in_real = np.sum(arr_1) / float(10 * coef_1)

                    if (mean_number_of_clicks_in_real > 0 and mean_number_of_clicks_in_real <= 0.1):
                        first_result_file.write(result)

                    elif (mean_number_of_clicks_in_real > 0.1 and mean_number_of_clicks_in_real <= 0.2):
                        second_result_file.write(result)

                    elif (mean_number_of_clicks_in_real > 0.2 and mean_number_of_clicks_in_real <= 0.3):
                        third_result_file.write(result)
                    else:
                        last_result_file.write(result)

                elif (real_session.query == pred_session.query and real_session.task == pred_session.task):
                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'

                    arr_1 = global_arr[(int)(real_session.query)]
                    coef_1 = (float)(global_arr_cnt[(int)(real_session.query)])

                    mean_number_of_clicks_in_real = np.sum(arr_1) / float(10 * coef_1)

                    if (mean_number_of_clicks_in_real > 0 and mean_number_of_clicks_in_real <= 0.1):
                        first_result_file.write(result)

                    elif (mean_number_of_clicks_in_real > 0.1 and mean_number_of_clicks_in_real <= 0.2):
                        second_result_file.write(result)

                    elif (mean_number_of_clicks_in_real > 0.2 and mean_number_of_clicks_in_real <= 0.3):
                        third_result_file.write(result)
                    else:
                        last_result_file.write(result)


                else:
                    real_JSON_line = real_test_JSON_file.next()
                    index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)

                    while (real_session.query != pred_session.query or real_session.task != pred_session.task):
                        print('real', index_in_real, 'pred', index_in_pred)
                        real_JSON_line = real_test_JSON_file.next()
                        index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)

                    fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                    mse = mean_squared_error(pred_clicks, real_clicks)

                    result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'

                    arr_1 = global_arr[(int)(real_session.query)]
                    coef_1 = (float)(global_arr_cnt[(int)(real_session.query)])

                    mean_number_of_clicks_in_real = np.sum(arr_1) / float(10 * coef_1)

                    if (mean_number_of_clicks_in_real > 0 and mean_number_of_clicks_in_real <= 0.1):
                        first_result_file.write(result)

                    elif (mean_number_of_clicks_in_real > 0.1 and mean_number_of_clicks_in_real <= 0.2):
                        second_result_file.write(result)

                    elif (mean_number_of_clicks_in_real > 0.2 and mean_number_of_clicks_in_real <= 0.3):
                        third_result_file.write(result)
                    else:
                        last_result_file.write(result)

        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')

    first_result_file.close()
    second_result_file.close()
    third_result_file.close()
    last_result_file.close()

save_metric_results_mean_clicks("second_simple_pred")