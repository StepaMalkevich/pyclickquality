import glob
import numpy as np
from pyclick.search_session import SearchSession


def get_session_from_JSON_line(line):
    entry_array = line.strip().split("\t")
    index, session_in_json = entry_array[0], entry_array[1]
    session = SearchSession(None).from_JSON(session_in_json)

    return index, session

def number_of_clicks(query_session):

    cc = np.sum(np.array(query_session.get_clicks()))

    return cc

def number_of_clicks_pred(query_session):
    cc = 0
    session_len = len(query_session.web_results)
    for i in range(session_len):
        if (query_session.web_results[i].click > 1):
            print("warning")
            cc += 1

    return cc

def show_statistics(model_name):

    dic = {}

    list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")

    for name in list_of_names:
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                cc = number_of_clicks(query_session)

                if (cc not in dic.keys()):
                    dic[cc] = 1
                else:
                    dic[cc] += 1



        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')


    print(dic)
    print(dic.values())


show_statistics("CCM_pred")
