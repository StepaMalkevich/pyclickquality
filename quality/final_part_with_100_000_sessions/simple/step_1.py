import os
import time
import numpy as np
import glob

from pyclick.click_models.CCM import CCM
from pyclick.click_models.DBN import DBN
from pyclick.click_models.task_centric.TaskCentricSearchSession import TaskCentricSearchSession
from pyclick.search_session.SearchResult import SearchResult


def binary_search(f, query):
    """
      Search query and JSON in file f.

      :returns: line with query and JSON or False.
      """
    r = os.fstat(f.fileno()).st_size
    l = 0
    x = int(query)

    while ((r - l) > 1):
        query = (r + l) >> 1

        f.seek(query)
        while f.read(1) != '\n':
            pass

        line = f.readline()
        data = line.split("\t")

        elem = int(data[0])

        if elem > x:
            r = query
        if elem < x:
            l = query

        if (elem == x):
            return line

    return False

def first_simple_simulator(query_session):
    """
   Always simulate no clicks.

   :returns: Vector of simulated clicks.
   """
    session_len = len(query_session.web_results)
    simulated_clicks = [0] * session_len

    return simulated_clicks


def second_simple_simulator(query_session):
    """
   Always simulate click on first position.

   :returns: Vector of simulated clicks.
   """
    session_len = len(query_session.web_results)
    simulated_clicks = [0] * session_len

    simulated_clicks[0] = 1

    return simulated_clicks


def third_simple_simulator(query_session):
    """
   Random choice between first and second.

   :returns: Vector of simulated clicks.
   """
    r = np.random.random()

    if (r < 0.5):
        return first_simple_simulator(query_session)
    else:
        return second_simple_simulator(query_session)


def simulating_user_clicks(click_model, query_session):
    """
   Simulates search sessions.

   :returns: Vector of simulated clicks.
   """
    simulated_clicks = []

    session_len = len(query_session.web_results)
    for i in range(session_len):
        query_session.web_results[i].click = 0

    for i in np.arange(session_len):
        P_r = click_model.get_conditional_click_probs(query_session)
        p = P_r[i]
        simulated_click = np.random.binomial(1, p, 1)[0]
        if (simulated_click == 1):
            query_session.web_results[i].click = 1

        simulated_clicks.append(simulated_click)

    return simulated_clicks


class SessionsIterator(object):
    def __init__(self, fname, session_max=None):
        self.f = open(fname, 'r')
        self.session_max = session_max
        self.prev_line = 'start'
        self.flag = False
        self.line_number = 0

        while (self.line_number < -1):
            read_line = self.f.readline()
            self.line_number += 1

    def __iter__(self):
        return self

    def next(self):
        sessions = []

        while True:
            if (self.flag):
                line = self.prev_line
                self.flag = False
            else:
                line = self.f.readline()
                self.line_number += 1

                if (self.line_number % 100000 == 0):
                    print self.line_number

            entry_array = line.strip().split("\t")

            if len(entry_array) == 4 and entry_array[1] == 'M':
                pass

            elif len(entry_array) >= 7 and (entry_array[2] == 'Q' or entry_array[2] == 'T'):

                if self.session_max and len(sessions) >= self.session_max:
                    self.prev_line = line
                    break

                task = entry_array[0]
                serp = entry_array[3]
                query = entry_array[4]
                urls_domains = entry_array[6:]
                session = TaskCentricSearchSession(task, query)

                results = []
                for url_domain in urls_domains:
                    result = url_domain.strip().split(',')[0]
                    url_domain = SearchResult(result, 0)
                    results.append(result)

                    session.web_results.append(url_domain)

                sessions.append(session)

            elif len(entry_array) == 5 and entry_array[2] == 'C':
                if entry_array[0] == task and entry_array[3] == serp:
                    clicked_result = entry_array[4]
                    if clicked_result in results:
                        index = results.index(clicked_result)
                        session.web_results[index].click = 1
            else:
                if not line:
                    if (sessions != []):
                        self.flag = False
                        return sessions

                    raise StopIteration

        if (sessions != []):
            self.flag = True
            return sessions

        raise StopIteration


session_counter = 0
start = time.time()

list_of_names = glob.glob("/Users/Stepan/Desktop/test_data/real_test_days_first_100_000/*")
for name in list_of_names:
    print(name)
    day = name[-6:-4]
    real_sessions_iter = SessionsIterator(name, 10000)
    file_with_predicted_session_first = open(
            "/Users/Stepan/Desktop/data_100_000/first_simple_pred_test_JSON_days/first_simple_pred_test_JSON_day_" + day + ".txt", 'a')

    file_with_predicted_session_second = open(
            "/Users/Stepan/Desktop/data_100_000/second_simple_pred_test_JSON_days/second_simple_pred_test_JSON_day_" + day + ".txt", 'a')

    for sessions in real_sessions_iter:
        for session in sessions:
            first_predicted_clicks = first_simple_simulator(session)
            session.change_clicks(first_predicted_clicks)
            file_with_predicted_session_first.write(str(session_counter) + "\t" + str(session) + "\n")


            second_predicted_clicks = second_simple_simulator(session)
            session.change_clicks(second_predicted_clicks)

            file_with_predicted_session_second.write(str(session_counter) + "\t" + str(session) + "\n")

            session_counter += 1

    file_with_predicted_session_first.close()
    file_with_predicted_session_second.close()

end = time.time()

print "Session counter is", session_counter
print "Work time is", (end - start) / 60, 'min'
