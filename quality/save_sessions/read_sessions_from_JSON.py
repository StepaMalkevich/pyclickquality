import glob
import numpy as np
from pyclick.search_session import SearchSession

def mae_first_last_clicks(pred, real):
    """
    Mean Absolute Error of first and last real and predicted clicks.

    :returns: Two MAE.
    """
    flag_pred = True
    flag_real = True

    r_first_pred = None
    r_first_real = None
    r_last_pred = None
    r_last_real = None

    for i in range(len(pred)):
        if (flag_pred and pred[i] == 1):
            r_first_pred = i + 1
            flag_pred = False
        if (flag_real and real[i] == 1):
            r_first_real = i + 1
            flag_real = False

        if ((not flag_pred) and pred[i] == 1):
            r_last_pred = i + 1
        if ((not flag_real) and real[i] == 1):
            r_last_real = i + 1

    if (r_first_real == None):
        return None, (r_first_pred, r_last_pred)

    if (r_first_pred == None):
        return None, (r_first_real, r_last_real)

    return abs(r_first_pred - r_first_real), abs(r_last_pred - r_last_real)


def mean_squared_error(pred, real):
    """
    Mean Squared Error of real and predicted clicks.

    :returns: MSE.
    """
    pred = np.array(pred)
    real = np.array(real)

    return ((pred - real) ** 2).mean(axis=0)


list_of_names = glob.glob("/Users/Stepan/Desktop/test_data/real_test_JSON_days/*")


def get_session_from_JSON_line(line):
    entry_array = line.strip().split("\t")
    index, session_in_json = entry_array[0], entry_array[1]
    session = SearchSession(None).from_JSON(session_in_json)

    return index, session



result_file = open("/Users/Stepan/Desktop/DBN_work_2/results_by_days/result_of_work.txt", 'w')

for name in list_of_names:
    print('begin in' + name)
    day = name[-6:-4]
    real_test_JSON_file = open(name, 'r')
    DBN_pred_test_JSON_file = open("/Users/Stepan/Desktop/test_data/model_pred_test_JSON_days/DBN_pred_test_JSON_day_" + day + ".txt", 'r')
    try:
        while True:
            real_JSON_line = real_test_JSON_file.next()
            pred_JSON_line = DBN_pred_test_JSON_file.next()

            index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)
            index_in_pred, pred_session = get_session_from_JSON_line(pred_JSON_line)

            real_clicks = real_session.get_clicks()
            pred_clicks = pred_session.get_clicks()

            if (index_in_real == index_in_pred):
                fst, snd = mae_first_last_clicks(pred_clicks, real_clicks)
                mse = mean_squared_error(pred_clicks, real_clicks)

                result = index_in_real + '\t' + str(fst) + '\t' + str(snd) + '\t' + str(mse) + '\n'
                result_file.write(result)
            else:
                print('real', index_in_real, 'pred', index_in_pred)

    except(StopIteration):
        print('exit from' + name)
    except(ValueError):
        print('value error in' + name, 'Exit from this')

result_file.close()


# for line in session_file:
#     entry_array = line.strip().split("\t")
#     index, session_in_json = entry_array[0], entry_array[1]
#     session = SearchSession(None).from_JSON(session_in_json)























