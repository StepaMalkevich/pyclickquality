file_with_query = open("/Users/Stepan/Desktop/query_for_UBM.txt", 'r')

model_file = open("/Users/Stepan/Desktop/models/UBM", 'r')

part_of_model_file = open("/Users/Stepan/Desktop/part_of_UBM", 'w')

query_set = set()

for line in file_with_query:
    query = line.strip()
    query_set.add(query)

for line in model_file:
    line = line.strip()
    UBM_query, _ = line.split("\t")

    if (UBM_query in query_set):
        part_of_model_file.write(line)

model_file.close()
part_of_model_file.close()
file_with_query.close()
