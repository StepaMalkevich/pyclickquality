session_file = open("/Users/Stepan/Desktop/data/train", "r")
test_file = open("/Users/Stepan/Desktop/data/test.txt", 'w')

flag = False
for line in session_file:
    entry_array = line.strip().split("\t")


    if len(entry_array) == 4 and entry_array[1] == 'M':
        day = (int)(entry_array[2])
        if (day <= 14):
            flag = True
            continue
        else:
            flag = False
    else:
        if (flag):
            continue
        else:
            test_file.write(line)
