real_test_file = open("/Users/Stepan/Desktop/train_data/train", "r")

flag = False
day = 0
for line in real_test_file:
    entry_array = line.strip().split("\t")


    if len(entry_array) == 4 and entry_array[1] == 'M':
        day = (int)(entry_array[2])
        if (day <= 14):
            flag = True
            continue
        else:
            flag = False
    else:
        if (flag):
            continue
        else:
            test_day_k = open("/Users/Stepan/Desktop/test_data/test_days/test_day_" + str(day) + ".txt", "a")
            test_day_k.write(line)