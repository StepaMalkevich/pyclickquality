#get from Ilya UBM data with first 100_000 session from every day to final part
import glob
from pyclick.click_models.task_centric.TaskCentricSearchSession import TaskCentricSearchSession
from pyclick.search_session.SearchResult import SearchResult


class SessionsIterator(object):
    def __init__(self, fname, session_max=None):
        self.f = open(fname, 'r')
        self.session_max = session_max
        self.prev_line = 'start'
        self.flag = False
        self.line_number = 0

        while (self.line_number < -1):
            read_line = self.f.readline()
            self.line_number += 1

    def __iter__(self):
        return self

    def next(self):
        sessions = []

        while True:
            if (self.flag):
                line = self.prev_line
                self.flag = False
            else:
                line = self.f.readline()
                self.line_number += 1

                if (self.line_number % 100000 == 0):
                    print self.line_number

            entry_array = line.strip().split("\t")

            if len(entry_array) == 4 and entry_array[1] == 'M':
                pass

            elif len(entry_array) >= 7 and (entry_array[2] == 'Q' or entry_array[2] == 'T'):

                if self.session_max and len(sessions) >= self.session_max:
                    self.prev_line = line
                    break

                task = entry_array[0]
                serp = entry_array[3]
                query = entry_array[4]
                urls_domains = entry_array[6:]
                session = TaskCentricSearchSession(task, query)

                results = []
                for url_domain in urls_domains:
                    result = url_domain.strip().split(',')[0]
                    url_domain = SearchResult(result, 0)
                    results.append(result)

                    session.web_results.append(url_domain)

                sessions.append(session)

            elif len(entry_array) == 5 and entry_array[2] == 'C':
                if entry_array[0] == task and entry_array[3] == serp:
                    clicked_result = entry_array[4]
                    if clicked_result in results:
                        index = results.index(clicked_result)
                        session.web_results[index].click = 1
            else:
                if not line:
                    if (sessions != []):
                        self.flag = False
                        return sessions

                    raise StopIteration

        if (sessions != []):
            self.flag = True
            return sessions

        raise StopIteration

list_of_names = glob.glob("/Users/Stepan/Desktop/test_data/real_test_days_first_100_000/*")
file_to_Ilya = open("/Users/Stepan/Desktop/query_from_UBM.txt", 'w')
set_q = set()

for name in list_of_names:
    print(name)
    day = name[-6:-4]
    real_sessions_iter = SessionsIterator(name, 10000)


    for sessions in real_sessions_iter:
        for session in sessions:
            query = session.query
            set_q.add(query)


for q in set_q:
    file_to_Ilya.write(q + "\n")

file_to_Ilya.close()
