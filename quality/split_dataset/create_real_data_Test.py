import glob

model_file = open("/Users/Stepan/Desktop/models/DBN_sorted", 'r')

def binary_search(a, x):
    l = 0
    r = len(a)

    while (r - l > 1):
        m = (r + l) >> 1

        if a[m] > x:
            r = m
        else:
            l = m

    if (a[l] != x):
        return -1
    else:
        return l



def find_not_in_train():
    query_ids_train = []
    for line in model_file:
        entry_array = line.strip().split("\t")
        query_id = int(entry_array[0])
        query_ids_train.append(query_id)



    test_file = open("/Users/Stepan/Desktop/test_data/test.txt", 'r')
    not_in_train_file = open("/Users/Stepan/Desktop/test_data/not_in_train.txt", 'w')


    cc = 0
    not_cc = 0
    for line in test_file:
        cc += 1
        if (cc % 1000000 == 0):
            print(cc)


        entry_array = line.strip().split("\t")


        if len(entry_array) >= 7 and (entry_array[2] == 'Q' or entry_array[2] == 'T'):
            session_id = entry_array[0]
            query = (int)(entry_array[4])
            res = binary_search(query_ids_train, query)
            if (res == -1):
                not_in_train_file.write(session_id + '\n')
                not_cc += 1


    print(cc)
    print(not_cc)
    not_in_train_file.close()


def delete_from_test_not_in_train():
    print('begin')
    test_file = open("/Users/Stepan/Desktop/test_data/test.txt", 'r')

    not_in_train_file = open("/Users/Stepan/Desktop/test_data/not_in_train.txt", 'r')

    real_test_file = open("/Users/Stepan/Desktop/test_data/real_test.txt", 'w')


    not_in_train_session_ids = []
    for line in not_in_train_file:
        session_id = (int)(line.strip())
        not_in_train_session_ids.append(session_id)

    print('not_in_train_session_ids list is ready')
    for line in test_file:
        entry_array = line.strip().split("\t")
        session_id = (int)(entry_array[0])

        res = binary_search(not_in_train_session_ids, session_id)
        if (res == -1):
            real_test_file.write(line)



    real_test_file.close()

def delete_from_test_not_in_train_by_days():
    print('begin')
    not_in_train_file = open("/Users/Stepan/Desktop/test_data/not_in_train.txt", 'r')

    not_in_train_session_ids = []
    for line in not_in_train_file:
        session_id = (int)(line.strip())
        not_in_train_session_ids.append(session_id)

    print('not_in_train_session_ids list is ready')

    list_of_names = glob.glob("/Users/Stepan/Desktop/test_data/test_days/*")
    for name in list_of_names:
        day = name[-6:-4]
        print(name)
        print(day)
        test_file_day = open(name, 'r')
        real_test_day = open("/Users/Stepan/Desktop/test_data/real_test_days/real_test_day_" + day + ".txt", "w")


        for line in test_file_day:
            entry_array = line.strip().split("\t")
            session_id = (int)(entry_array[0])

            res = binary_search(not_in_train_session_ids, session_id)
            if (res == -1):
                real_test_day.write(line)



        real_test_day.close()


delete_from_test_not_in_train_by_days()


