from scipy import stats
import glob

list_of_file_path =  glob.glob("/Users/Stepan/Desktop/CCM_work/result/*.txt")


list_r_first_1 = []
list_r_first_2 = []
list_r_last_1 = []
list_r_last_2 = []
list_mse_1 = []
list_mse_2 = []

print 'start CCM'
for path in list_of_file_path:
    print path
    file = open(path, 'r')
    for line in file:
        line = line.strip()
        lines = line.split("\t")
        query, res, mse = lines

        if (res != 'None'):
            data = res.split(', ')
            r_first = (float)(data[0][1])
            list_r_first_1.append(r_first)

            r_last = (float)(data[1][0])
            list_r_last_1.append(r_last)


        mse = (float)(mse)
        list_mse_1.append(mse)


print 'start DBN'

list_of_file_path = glob.glob("/Users/Stepan/Desktop/DBN_work/result/*.txt")


flag = True

for path in list_of_file_path:
    print path
    file = open(path, 'r')
    for line in file:
        line = line.strip()
        lines = line.split("\t")
        query, res_1, res_2, mse = lines

        if (flag):
            if (res_1 == 'None'):

                if (res_2 == '(None, None)'):
                    list_r_first_2.append(0)
                    list_r_last_2.append(0)
                else:

                    data = res_2.split(', ')

                    r_first = (float)(data[0][1])
                    r_last = (float)(data[1][0])

                    list_r_first_2.append(r_first)
                    list_r_last_2.append(r_last)



            else:
                r_first = (float)(res_1)
                r_last = (float)(res_2)

                list_r_first_2.append(r_first)
                list_r_last_2.append(r_last)



        else:
            if (res_1 == 'None'):
                pass
            else:
                r_first = (float)(res_1)
                r_last = (float)(res_2)

                list_r_first_2.append(r_first)
                list_r_last_2.append(r_last)



        mse = (float)(mse)
        list_mse_2.append(mse)



print 'r_first:', stats.ttest_ind(list_r_first_1, list_r_first_2)
print 'r_last:', stats.ttest_ind(list_r_last_1, list_r_last_2)
print 'mse:', stats.ttest_ind(list_mse_1, list_mse_2)