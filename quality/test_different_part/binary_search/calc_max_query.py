import glob
import numpy as np
from pyclick.search_session import SearchSession


def get_session_from_JSON_line(line):
    entry_array = line.strip().split("\t")
    index, session_in_json = entry_array[0], entry_array[1]
    session = SearchSession(None).from_JSON(session_in_json)

    return index, session


def show_statistics(model_name):

    max_query = 0

    list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")

    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test.txt", "w")

    q_set = set()
    for name in list_of_names:
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                query = int(query_session.query)
                q_set.add(query)

                if (query > max_query):
                    max_query = query



        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')


    print(max_query)

    for elem in q_set:
        file_with_query_from_test.write(str(elem) + "\n")

def show_statistics_freq(model_name):

    max_query = 0

    list_of_names = glob.glob("/Users/Stepan/Desktop/data_100_000/" + model_name + "_test_JSON_days/*")

    file_with_query_from_test = open("/Users/Stepan/Desktop/query_from_test_with_freq.txt", "w")

    freq_dic = {}
    for name in list_of_names:
        print('Enter in' + name)
        JSON_file = open(name, 'r')

        try:
            while True:
                JSON_line = JSON_file.next()
                index, query_session = get_session_from_JSON_line(JSON_line)

                query = int(query_session.query)

                if (query not in freq_dic.keys()):
                    freq_dic[query] = 0
                else:
                    freq_dic[query] += 1

        except(StopIteration):
            print('Exit from' + name)
        except(ValueError):
            print('value error in' + name, 'Exit from this')


    print(max_query)

    for elem in freq_dic.keys():
        file_with_query_from_test.write(str(elem)+"\t"+str(freq_dic[elem]) + "\n")


show_statistics_freq("real")
