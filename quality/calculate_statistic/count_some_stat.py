from pyclick.search_session.SearchResult import SearchResult
from pyclick.click_models.task_centric.TaskCentricSearchSession import TaskCentricSearchSession


class SessionsIterator(object):
    def __init__(self, fname, session_max=None):
        self.f = open(fname, 'r')
        self.session_max = session_max
        self.prev_line = 'start'
        self.flag = False
        self.line_number = 0

        while (self.line_number < -1):
            read_line = self.f.readline()
            self.line_number += 1

    def __iter__(self):
        return self

    def next(self):
        sessions = []

        while True:
            if (self.flag):
                line = self.prev_line
                self.flag = False
            else:
                line = self.f.readline()
                self.line_number += 1

                if (self.line_number % 100000 == 0):
                    print self.line_number

            entry_array = line.strip().split("\t")

            if len(entry_array) == 4 and entry_array[1] == 'M':
                pass

            elif len(entry_array) >= 7 and (entry_array[2] == 'Q' or entry_array[2] == 'T'):

                if self.session_max and len(sessions) >= self.session_max:
                    self.prev_line = line
                    break

                task = entry_array[0]
                serp = entry_array[3]
                query = entry_array[4]
                urls_domains = entry_array[6:]
                session = TaskCentricSearchSession(task, query)

                results = []
                for url_domain in urls_domains:
                    result = url_domain.strip().split(',')[0]
                    url_domain = SearchResult(result, 0)
                    results.append(result)

                    session.web_results.append(url_domain)

                sessions.append(session)

            elif len(entry_array) == 5 and entry_array[2] == 'C':
                if entry_array[0] == task and entry_array[3] == serp:
                    clicked_result = entry_array[4]
                    if clicked_result in results:
                        index = results.index(clicked_result)
                        session.web_results[index].click = 1
            else:
                if not line:
                    if (sessions != []):
                        self.flag = False
                        return sessions

                    raise StopIteration

        if (sessions != []):
            self.flag = True
            return sessions

        raise StopIteration


all_session_counter = 0
without_clicks_counter = 0
one_click_counter = 0
first_one_click_counter = 0
sessions_iter = SessionsIterator('/Users/Stepan/Desktop/test_data/real_test.txt', 1000)
for sessions in sessions_iter:
    all_session_counter += len(sessions)
    for session in sessions:
        is_any_clicks = False
        is_first_click = False
        if (session.web_results[0].click == 1):
            is_first_click = True

        for i in range(1, len(session.web_results)):
            if (session.web_results[i].click == 1):
                is_any_clicks = True
                break

        if ((not is_first_click) and (not is_any_clicks)):
            without_clicks_counter += 1

        if (is_first_click and (not is_any_clicks)):
            one_click_counter += 1

        if ((not is_first_click) and is_any_clicks):
            one_click_counter += 1

        if (is_first_click and (not is_any_clicks)):
            first_one_click_counter += 1

print("all_session_counter", all_session_counter)
print("without_clicks_counter", without_clicks_counter)
print("one_click_counter", one_click_counter)
print("first_one_click_counter", first_one_click_counter)
