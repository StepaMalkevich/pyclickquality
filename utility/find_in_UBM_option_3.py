import os
import time
import numpy as np
import glob

def binary_search(f, query):
    """
      Search query and JSON in file f.

      :returns: line with query and JSON or False.
      """
    r = os.fstat(f.fileno()).st_size
    l = 0
    x = int(query)

    while ((r - l) > 1):
        query = (r + l) >> 1

        f.seek(query)
        while f.read(1) != '\n':
            pass

        line = f.readline()
        data = line.split("\t")

        elem = int(data[0])

        if elem > x:
            r = query
        if elem < x:
            l = query

        if (elem == x):
            return line

    return False


sorted_model_file = open("/UBM_sorted", 'r')
file_with_query = open("/query_for_UBM.txt", 'r')

part_of_sorted_model_file = open("/part_of_UBM_sorted", 'w')

for line in file_with_query:
    query = line.strip()
    line = binary_search(sorted_model_file, query)

    if (line == False):
        #This should never happen!!!
        print(query)
        continue

    part_of_sorted_model_file.write(line)

sorted_model_file.close()
part_of_sorted_model_file.close()
file_with_query.close()





