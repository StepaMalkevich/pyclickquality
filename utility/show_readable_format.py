from pyclick.search_session import SearchSession

def get_session_from_JSON_line(line):
    entry_array = line.strip().split("\t")
    index, session_in_json = entry_array[0], entry_array[1]
    session = SearchSession(None).from_JSON(session_in_json)

    return index, session


def show_days(result_file_path, day):
    result_file = open(result_file_path, "a")
    day = str(day)
    result_file.write("day " + day + ":" + "\n")
    real_test_JSON_file = open("data/real_test_JSON_days/real_test_JSON_day_" + day + ".txt", 'r')
    DBN_pred_test_JSON_file = open("data/model_pred_test_JSON_days/DBN_pred_test_JSON_day_" + day + ".txt", 'r')

    try:
        while True:
            real_JSON_line = real_test_JSON_file.next()
            pred_JSON_line = DBN_pred_test_JSON_file.next()

            index_in_real, real_session = get_session_from_JSON_line(real_JSON_line)
            index_in_pred, pred_session = get_session_from_JSON_line(pred_JSON_line)

            real_clicks = real_session.get_clicks()
            pred_clicks = pred_session.get_clicks()

            if (real_session.query == pred_session.query):
                result_file.write(str(real_clicks) + "\n")
                result_file.write(str(pred_clicks) + "\n")
                result_file.write("\n")

    except(StopIteration):
        print('Exit from ' + day)
    except(ValueError):
        print('value error in' + day, 'Exit from this')

    result_file.close()


result_file_path = "data/readable_format_real_DBN/readable_format_real_DBN.txt"
for i in range(15, 28):
    show_days(result_file_path, i)